<html>
<body>
<?php
/*After submitting the html form, it will works.Because the
html code contains the form tag in which we can define the action
(validatename.php) and method(post).*/?>

<?php
$nameErr=$emailErr=$genderErr="";
$name=$email=$gender="";
if($_SERVER["REQUEST_METHOD"]=="POST")
{
if(empty($_POST["name"]))
{
$nameErr="Name is required";
}
else
{
$name=test_input($_POST["name"]);
if(!preg_match("/^[a-zA-Z-']*$/",$name))
{
$nameErr="only letters and white space allowed";
}
}
if(empty($_POST["email"]))
{
$emailErr="Email is required";
}
else
{
$email=test_input($_POST["email"]);
if(!filter_var($email,FILTER_VALIDATE_EMAIL))
{
$emailErr="Invalid email format";
}
}
?>
</body>
</html>